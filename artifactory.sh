#!/bin/bash

# Autor: Daniel Oberti
revision="2020-07-01"

repo_url="http://<something>/artifactory"

### START DEFAULTS ###
token=""
tgt_repo=""
OPTION="upfile"
VERBOSE=0
THEUSER=$USER
FECHA=`date +"%Y%m%d_%H%M"`
### END DEFAULTS ###

function showhelp()
{
        echo "######### ARTIFACTORY COMMAND #########"
        echo "USAGE MODE:"
        echo "<this> [ARGUMENTOS]"
        echo
        echo "ARGUMENTS:"
        echo "          -o/--option: Indicate the option to do (Default: 'upfile')."
        echo "                       Values: upfile|updir"
        echo
        echo "          --tgt_repo <tgt_repo>:  (for example: --tgt_repo <something>/file)."
        echo "                                       something like $repo_url/<tgt_repo>"
        echo
        echo "          -f|--filename <filename>:  (it is case sensitive)."
        echo
        echo "          -d/--dir <dirname>: To indicate dirname"
        echo
        echo "          --version: To see command version."
        echo
        echo "          v/--verbose: Debug mode"
        echo
        echo "          -t/--token <token>: To indicate ARTIFACTORY token"
        echo
        echo "###################################"
        exit 0
}

ARGS=$(getopt -q -o "o:u:d:f:t: vh" -l "option:,user:,dir:,filename:,token:,tgt_repo:,verbose,help,version" -n "parameters" -- "$@");
eval set -- "$ARGS";

while [ $# -gt 0 ]; do
  case "$1" in
    --version)
      echo "Version: $revision"
      exit 0
      ;;
    -v|--verbose)
      VERBOSE=1
      ;;
    -u|--user)
      THEUSER="$2"
      ;;
    --tgt_repo)
      tgt_repo="$2"
      shift;
      ;;
    -f|--file)
      filename="$2"
      shift;
      ;;
    -d|--dir)
      dir="$2"
      shift;
      ;;
    -t|--token)
      token="$2"
      shift;
      ;;
    -o|--option)
      OPTION="$2"
      shift;
      ;;
    -h|--help)
      showhelp
      exit 0
      shift;
      ;;
    --)
      shift;
      break;
      ;;
  esac
  shift
done


echo "OPTION: $OPTION"

#############################
if [ "$OPTION" == "upfile" ]; then
    ### START CHECKS ###
    if [ "$filename" == "" ]; then
        echo "You must indicate a filename (-f|--filename <FILE>)"
        echo "Please try again"
        exit 1
    fi
    if [ "$tgt_repo" == "" ]; then
        echo "You must indicate a tgt_repo (--tgt_repo <tgt_repo>)"
        echo "(for example: --tgt_repo <something>/file       )"
        echo "something like $repo_url/<tgt_repo>"
        echo "Please try again"
        exit 1
    fi
    if [ "$token" == "" ]; then
        echo "You must indicate an Artifactory token"
        echo "Usage: -t|--token <TOKEN>"
        echo "Please try again"
        exit 1
    fi

    if [ -z "$filename" ]; then echo "Please specify a file to upload!"; exit 1; fi
    if [ ! -x "`which sha1sum`" ]; then echo "You need to have the 'sha1sum' command in your path."; exit 1; fi
    ### END CHECKS ###

    sha1=$(sha1sum "$filename")
    sha1="${sha1:0:40}"
    printf "\n\nUploading '$f' (cs=${sha1}) to '${repo_url}/${tgt_repo}/${filename}'"
    status=$(curl -k -u${token} -X PUT -H "X-Checksum-Deploy:true" -H "X-Checksum-Sha1:$sha1" --write-out %{http_code} --silent --output /dev/null "${repo_url}/${tgt_repo}/${filename}")
    echo "status=$status"
    # No checksum found - deploy + content


    [ ${status} -eq 404 ] && {
        curl -k -u${token} -H "X-Checksum-Sha1:$sha1" -T "$filename" "${repo_url}/${tgt_repo}/${filename}"
        echo "Success!"
        exit 0
    }


fi
#############################
#############################

# Recursively deploys folder content. Attempt checksum deploy first to optimize upload time.
if [ "$OPTION" == "updir" ]; then
    ### START CHECKS ###
    if [ "$dir" == "" ]; then
        echo "You must indicate a directory (-d|--dir <DIR>)"
        echo "Please try again"
        exit 1
    fi
    if [ "$tgt_repo" == "" ]; then
        echo "You must indicate a tgt_repo (--tgt_repo <tgt_repo>)"
        echo "(for example: --tgt_repo <something>/file       )"
        echo "something like $repo_url/<tgt_repo>"
        echo "Please try again"
        exit 1
    fi
    if [ "$token" == "" ]; then
        echo "You must indicate an Artifactory token"
        echo "Usage: -t|--token <TOKEN>"
        echo "Please try again"
        exit 1
    fi
    if [ -z "$dir" ]; then echo "Please specify a dir to upload!"; exit 1; fi
    if [ ! -x "`which sha1sum`" ]; then echo "You need to have the 'sha1sum' command in your path."; exit 1; fi
    ### END CHECKS ###

curl -k -u${token} -X PUT "${repo_url}/${tgt_repo}/${dir}"

# Upload by checksum all files from the source dir to the target repo
find "$dir" -type f | sort | while read f; do
    rel="$(echo "$f" | sed -e "s#$dir##" -e "s# /#/#")";
    sha1=$(sha1sum "$f")
    sha1="${sha1:0:40}"
    printf "\n\nUploading '$f' (cs=${sha1}) to '${repo_url}/${tgt_repo}/${dir}/${rel}'"
    #status=$(curl -k -u $user:$pass -X PUT -H "X-Checksum-Deploy:true" -H "X-Checksum-Sha1:$sha1" --write-out %{http_code} --silent --output /dev/null "${repo_url}/${tgt_repo}/${rel}")
    status=$(curl -k -u${token} -X PUT -H "X-Checksum-Deploy:true" -H "X-Checksum-Sha1:$sha1" --write-out %{http_code} --silent --output /dev/null "${repo_url}/${tgt_repo}/${dir}/${rel}")
    echo "status=$status"
    # No checksum found - deploy + content
    [ ${status} -eq 404 ] && {
        #curl -k -u $user:$pass -H "X-Checksum-Sha1:$sha1" -T "$f" "${repo_url}/${tgt_repo}/${rel}"
        curl -k -u${token} -H "X-Checksum-Sha1:$sha1" -T "$f" "${repo_url}/${tgt_repo}/${dir}/${rel}"
    }
done
fi
#############################


exit 1
