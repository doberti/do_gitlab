# pip install python-gitlab
import gitlab
# pip install dohq-artifactory
from artifactory import ArtifactoryPath
import os

'''
setenv GITLAB_URL <something>
setenv GITLAB_TOKEN <something>
setenv GITLAB_PROJECT_GROUP <something>
setenv GITLAB_PROJECT_NAME <something>
setenv GITLAB_URL <something> e.g.: https://artifactory.algo.com/artifactory/
setenv ARTIF_REPO <something>
setenv ARTIF_USER <something>
setenv ARTIF_PASS <something>
'''
GITLAB_URL = os.environ['GITLAB_URL']
GITLAB_TOKEN = os.environ['GITLAB_TOKEN']
GITLAB_PROJECT_GROUP = os.environ['GITLAB_PROJECT_GROUP']
GITLAB_PROJECT_NAME = os.environ['GITLAB_PROJECT_NAME']
ARTIF_URL = os.environ['ARTIF_URL']
ARTIF_REPO = os.environ['ARTIF_REPO']
ARTIF_USER = os.environ['ARTIF_USER']
ARTIF_PASS = os.environ['ARTIF_PASS']

aql = ArtifactoryPath(ARTIF_URL, auth=(ARTIF_USER,ARTIF_PASS))
gl = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)

group = gl.groups.get(5)
projects = group.projects.list()
#projects = gl.projects.list()
for project in projects:
    print("{}:{}".format(project.id, project.name) )
    if project.name == "<something>":
        print( project.tags )
        #print( project.releases.list() )
    '''
    releases = project.releases.list()
    for release in releases:
        print("{}:{}".format(release.id, release.id) )
        artifacts = aql.aql("items.find",{
                 "type":"file",
                 "repo":ARTIF_REPO,
                 "path":"{}*".format(release)
               })
        print(artifacts)
        print("*"*100)
    '''

exit(0)
