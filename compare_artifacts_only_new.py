# pip install python-gitlab
import gitlab
# pip install dohq-artifactory
from artifactory import ArtifactoryPath
import time
import os

'''
setenv GITLAB_URL "<something>"
setenv GITLAB_TOKEN "<something>"
setenv GITLAB_PROJECT_GROUP "<something>"
setenv GITLAB_PROJECT_NAME "<something>"

setenv OLD_ARTIF_URL "<something>"
setenv OLD_ARTIF_REPO "<something>"
setenv OLD_ARTIF_USER "<something>"
setenv OLD_ARTIF_PASS "<something>"

setenv ARTIF_URL "<something>"
setenv ARTIF_USER "<something>"
setenv ARTIF_PASS "<something>"
'''


GITLAB_URL = os.environ['GITLAB_URL']
GITLAB_TOKEN = os.environ['GITLAB_TOKEN']
GITLAB_PROJECT_GROUP = os.environ['GITLAB_PROJECT_GROUP']
GITLAB_PROJECT_NAME = os.environ['GITLAB_PROJECT_NAME']

OLD_ARTIF_URL = os.environ['OLD_ARTIF_URL']
OLD_ARTIF_REPO = os.environ['OLD_ARTIF_REPO']
OLD_ARTIF_USER = os.environ['OLD_ARTIF_USER']
OLD_ARTIF_PASS = os.environ['OLD_ARTIF_PASS']

ARTIF_URL = os.environ['ARTIF_URL']
ARTIF_REPO = "cdsp-firmware-{}-generic-preprod-dc5".format(OLD_ARTIF_REPO)
ARTIF_USER = os.environ['ARTIF_USER']
ARTIF_PASS = os.environ['ARTIF_PASS']


f = open("{}_only_new.revision".format(OLD_ARTIF_REPO),mode="w",encoding="utf-8")

old_aql = ArtifactoryPath(OLD_ARTIF_URL, auth=(OLD_ARTIF_USER,OLD_ARTIF_PASS))
aql = ArtifactoryPath(ARTIF_URL, auth=(ARTIF_USER,ARTIF_PASS))
gl = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)

artifacts = aql.aql("items.find",{
             "type":"file",
             "repo":ARTIF_REPO
             #"path":"*.zip"
           })
count = 0


for artifact in artifacts:
    old_artifacts = old_aql.aql("items.find",{
             "type":"file",
             "repo":OLD_ARTIF_REPO,
             "path":"{}".format(artifact['path']),
             "name":"{}".format(artifact['name'])
           })
    for old_artifact in old_artifacts:
        if old_artifact['name'] != artifact['name']:
            print("revisar este caso raro")
            exit(1)

    if len(old_artifacts)==0 or len(str(old_artifacts))<4:
        count += 1
        f.write("\n")
        print()
        print("new:{}".format(artifact['name']))
        f.write("new:{}/{}\n".format(artifact['path'],artifact['name']))

f.close()
print("total: {}".format(count))

exit(0)
